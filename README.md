# Not you are

Simple c++ project, created for testing different DevOps programs.

Running program with some command line argument, like "hadsome guy", will result in output "Not, you are handsome guy".

## Building:

1) Execute `sudo install_requirements.sh` or install c++ compiler and cmake manually

2) Execute `update_submodules.sh`

3) Execute `build.sh`

4) Execute `run.sh` with some arguments (like "handsome guy")
