#!/bin/bash

# Requirements installation script for ubuntu

set -e

apt-get update
apt-get install -y git
apt-get install -y clang
apt-get install -y cmake
