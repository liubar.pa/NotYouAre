FROM ubuntu:latest

WORKDIR /NotYouAre

COPY install_requirements.sh .
RUN bash install_requirements.sh

COPY .git ./.git
COPY update_submodules.sh .
RUN bash update_submodules.sh

COPY src ./src
COPY include ./include
COPY test ./test
COPY CMakeLists.txt .
COPY build.sh .
RUN bash build.sh

COPY run.sh .

ENTRYPOINT ["/NotYouAre/run.sh"]
