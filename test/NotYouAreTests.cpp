#include <gtest/gtest.h>

#include <NotYouAre.hpp>

#include "CheckOutput.hpp"

template<size_t ArraySize>
void callNotYouAre(const char* (&argv)[ArraySize])
{
    printNotYouAre(std::size(argv), argv);
}

TEST(printNotYouAre, Handsome)
{
    checkOutput([]() {
        const char* argv[] = {"./Main", "handsome"};
        callNotYouAre(argv);
    }, "Not, you are handsome\n");
}

TEST(printNotYouAre, Boring)
{
    checkOutput([]() {
        const char* argv[] = {"./Main"};
        callNotYouAre(argv);
    }, "You are so boring, please give me some command line arguments\n");
}

TEST(printNotYouAre, MultipleArguments)
{
    checkOutput([]() {
        const char*
                argv[] = {"./Main", "the", "most", "handsome", "guy", "I", "have", "ever", "seen"};

        callNotYouAre(argv);
    }, "Not, you are the most handsome guy I have ever seen\n");
}
