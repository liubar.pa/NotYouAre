#!/bin/bash

set -e

rm -rf build
rm -rf lib
rm -rf bin
