#!/bin/bash

set -e

mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..

if [[($@ == *'--test'*)]]
then
    cmake --build . --target Tests -j 16
else
    cmake --build . --target Main -j 16
fi
