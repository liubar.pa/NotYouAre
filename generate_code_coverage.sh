#!/bin/bash

set -e

mkdir -p build
cd build
cmake -DCODE_COVERAGE=ON -DCMAKE_BUILD_TYPE=Debug ..
cmake --build . --target ccov -j 16
open ccov/Tests/index.html
